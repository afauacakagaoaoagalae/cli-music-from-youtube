Just a simple script to use mpv and youtube-dl to search and play music in your terminal.

If anyone would like to edit/update this, then feel free to.

Requires 'mpv' and 'youtube-dl'.

On Debian-based distros - 'sudo apt install mpv youtube-dl'.

On Arch-based distros - 'sudo pacman -S mpv youtube-dl'.

On Slackware-based distros - 'sudo slapt-get -i mpv youtube-dl'.

On Fedora-based distros - 'sudo yum install mpv youtube-dl'.